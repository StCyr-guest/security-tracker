CVE-2023-37378
	[bookworm] - nsis 3.08-3+deb12u1
CVE-2023-3153
	[bookworm] - ovn 23.03.1-1~deb12u1
CVE-2023-43040
	[bookworm] - ceph 16.2.11+ds-2+deb12u1
CVE-2023-40481
	[bookworm] - 7zip 22.01+dfsg-8+deb12u1
CVE-2023-31102
	[bookworm] - 7zip 22.01+dfsg-8+deb12u1
CVE-2023-39350
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39351
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39352
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39353
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39354
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39356
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40181
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40186
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40188
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40567
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40569
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40589
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-4039
	[bookworm] - gcc-12 12.2.0-14+deb12u1
CVE-2023-34410
	[bookworm] - qtbase-opensource-src 5.15.8+dfsg-11+deb12u1
CVE-2023-37369
	[bookworm] - qtbase-opensource-src 5.15.8+dfsg-11+deb12u1
CVE-2023-38197
	[bookworm] - qtbase-opensource-src 5.15.8+dfsg-11+deb12u1
CVE-2023-49208
	[bookworm] - glewlwyd 2.7.5-3+deb12u1
CVE-2024-25715
	[bookworm] - glewlwyd 2.7.5-3+deb12u1
CVE-2024-20290
	[bookworm] - clamav 1.0.5+dfsg-1~deb12u1
CVE-2024-20328
	[bookworm] - clamav 1.0.5+dfsg-1~deb12u1
CVE-2024-25189
	[bookworm] - libjwt 1.10.2-1+deb12u1
CVE-2023-50387
	[bookworm] - systemd 252.23-1~deb12u1
CVE-2023-50868
	[bookworm] - systemd 252.23-1~deb12u1
CVE-2024-27354
	[bookworm] - php-phpseclib 2.0.42-1+deb12u2
	[bookworm] - php-phpseclib3 3.0.19-1+deb12u3
	[bookworm] - phpseclib 1.0.20-1+deb12u2
CVE-2024-27355
	[bookworm] - php-phpseclib 2.0.42-1+deb12u2
	[bookworm] - php-phpseclib3 3.0.19-1+deb12u3
	[bookworm] - phpseclib 1.0.20-1+deb12u2
CVE-2024-0074
	[bookworm] - nvidia-graphics-drivers-tesla-470 470.239.06-1~deb12u1
CVE-2024-0078
	[bookworm] - nvidia-graphics-drivers-tesla-470 470.239.06-1~deb12u1
CVE-2022-42265
	[bookworm] - nvidia-graphics-drivers-tesla-470 470.239.06-1~deb12u1
CVE-2023-5678
	[bookworm] - openssl 3.0.13-1~deb12u1
CVE-2023-6129
	[bookworm] - openssl 3.0.13-1~deb12u1
CVE-2023-6237
	[bookworm] - openssl 3.0.13-1~deb12u1
CVE-2024-0727
	[bookworm] - openssl 3.0.13-1~deb12u1
CVE-2024-2182
	[bookworm] - ovn 23.03.1-1~deb12u2
CVE-2021-3420
	[bookworm] - newlib 3.3.0-2
CVE-2024-28054
	[bookworm] - amavisd-new 1:2.13.0-3+deb12u1
CVE-2023-52159
	[bookworm] - gross 1.0.2-4.1~deb12u1
CVE-2023-39368
	[bookworm] - intel-microcode 3.20240312.1~deb12u1
CVE-2023-38575
	[bookworm] - intel-microcode 3.20240312.1~deb12u1
CVE-2023-28746
	[bookworm] - intel-microcode 3.20240312.1~deb12u1
CVE-2023-22655
	[bookworm] - intel-microcode 3.20240312.1~deb12u1
CVE-2023-43490
	[bookworm] - intel-microcode 3.20240312.1~deb12u1
CVE-2023-2176
	[bookworm] - linux 6.1.82-1
CVE-2023-28746
	[bookworm] - linux 6.1.82-1
CVE-2023-52429
	[bookworm] - linux 6.1.82-1
CVE-2023-52434
	[bookworm] - linux 6.1.82-1
CVE-2023-52435
	[bookworm] - linux 6.1.82-1
CVE-2023-52583
	[bookworm] - linux 6.1.82-1
CVE-2023-52584
	[bookworm] - linux 6.1.82-1
CVE-2023-52587
	[bookworm] - linux 6.1.82-1
CVE-2023-52588
	[bookworm] - linux 6.1.82-1
CVE-2023-52589
	[bookworm] - linux 6.1.82-1
CVE-2023-52593
	[bookworm] - linux 6.1.82-1
CVE-2023-52594
	[bookworm] - linux 6.1.82-1
CVE-2023-52595
	[bookworm] - linux 6.1.82-1
CVE-2023-52597
	[bookworm] - linux 6.1.82-1
CVE-2023-52598
	[bookworm] - linux 6.1.82-1
CVE-2023-52599
	[bookworm] - linux 6.1.82-1
CVE-2023-52600
	[bookworm] - linux 6.1.82-1
CVE-2023-52601
	[bookworm] - linux 6.1.82-1
CVE-2023-52602
	[bookworm] - linux 6.1.82-1
CVE-2023-52603
	[bookworm] - linux 6.1.82-1
CVE-2023-52604
	[bookworm] - linux 6.1.82-1
CVE-2023-52606
	[bookworm] - linux 6.1.82-1
CVE-2023-52607
	[bookworm] - linux 6.1.82-1
CVE-2023-52616
	[bookworm] - linux 6.1.82-1
CVE-2023-52617
	[bookworm] - linux 6.1.82-1
CVE-2023-52618
	[bookworm] - linux 6.1.82-1
CVE-2023-52619
	[bookworm] - linux 6.1.82-1
CVE-2023-52620
	[bookworm] - linux 6.1.82-1
CVE-2023-52621
	[bookworm] - linux 6.1.82-1
CVE-2023-52622
	[bookworm] - linux 6.1.82-1
CVE-2023-52623
	[bookworm] - linux 6.1.82-1
CVE-2023-52630
	[bookworm] - linux 6.1.82-1
CVE-2023-52631
	[bookworm] - linux 6.1.82-1
CVE-2023-52632
	[bookworm] - linux 6.1.82-1
CVE-2023-52633
	[bookworm] - linux 6.1.82-1
CVE-2023-52635
	[bookworm] - linux 6.1.82-1
CVE-2023-52637
	[bookworm] - linux 6.1.82-1
CVE-2023-52638
	[bookworm] - linux 6.1.82-1
CVE-2023-52639
	[bookworm] - linux 6.1.82-1
CVE-2023-52640
	[bookworm] - linux 6.1.82-1
CVE-2023-52641
	[bookworm] - linux 6.1.82-1
CVE-2023-6270
	[bookworm] - linux 6.1.82-1
CVE-2023-7042
	[bookworm] - linux 6.1.82-1
CVE-2024-0340
	[bookworm] - linux 6.1.82-1
CVE-2024-0841
	[bookworm] - linux 6.1.82-1
CVE-2024-1151
	[bookworm] - linux 6.1.82-1
CVE-2024-22099
	[bookworm] - linux 6.1.82-1
CVE-2024-23850
	[bookworm] - linux 6.1.82-1
CVE-2024-23851
	[bookworm] - linux 6.1.82-1
CVE-2024-26581
	[bookworm] - linux 6.1.82-1
CVE-2024-26582
	[bookworm] - linux 6.1.82-1
CVE-2024-26583
	[bookworm] - linux 6.1.82-1
CVE-2024-26586
	[bookworm] - linux 6.1.82-1
CVE-2024-26590
	[bookworm] - linux 6.1.82-1
CVE-2024-26593
	[bookworm] - linux 6.1.82-1
CVE-2024-26600
	[bookworm] - linux 6.1.82-1
CVE-2024-26601
	[bookworm] - linux 6.1.82-1
CVE-2024-26602
	[bookworm] - linux 6.1.82-1
CVE-2024-26603
	[bookworm] - linux 6.1.82-1
CVE-2024-26606
	[bookworm] - linux 6.1.82-1
CVE-2024-26621
	[bookworm] - linux 6.1.82-1
CVE-2024-26622
	[bookworm] - linux 6.1.82-1
CVE-2024-26625
	[bookworm] - linux 6.1.82-1
CVE-2024-26626
	[bookworm] - linux 6.1.82-1
CVE-2024-26627
	[bookworm] - linux 6.1.82-1
CVE-2024-26629
	[bookworm] - linux 6.1.82-1
CVE-2024-26639
	[bookworm] - linux 6.1.82-1
CVE-2024-26640
	[bookworm] - linux 6.1.82-1
CVE-2024-26641
	[bookworm] - linux 6.1.82-1
CVE-2024-26651
	[bookworm] - linux 6.1.82-1
CVE-2024-26659
	[bookworm] - linux 6.1.82-1
CVE-2024-26660
	[bookworm] - linux 6.1.82-1
CVE-2024-26663
	[bookworm] - linux 6.1.82-1
CVE-2024-26664
	[bookworm] - linux 6.1.82-1
CVE-2024-26665
	[bookworm] - linux 6.1.82-1
CVE-2024-26667
	[bookworm] - linux 6.1.82-1
CVE-2024-26671
	[bookworm] - linux 6.1.82-1
CVE-2024-26673
	[bookworm] - linux 6.1.82-1
CVE-2024-26675
	[bookworm] - linux 6.1.82-1
CVE-2024-26676
	[bookworm] - linux 6.1.82-1
CVE-2024-26679
	[bookworm] - linux 6.1.82-1
CVE-2024-26680
	[bookworm] - linux 6.1.82-1
CVE-2024-26681
	[bookworm] - linux 6.1.82-1
CVE-2024-26684
	[bookworm] - linux 6.1.82-1
CVE-2024-26685
	[bookworm] - linux 6.1.82-1
CVE-2024-26686
	[bookworm] - linux 6.1.82-1
CVE-2024-26687
	[bookworm] - linux 6.1.82-1
CVE-2024-26688
	[bookworm] - linux 6.1.82-1
CVE-2024-26689
	[bookworm] - linux 6.1.82-1
CVE-2024-26695
	[bookworm] - linux 6.1.82-1
CVE-2024-26696
	[bookworm] - linux 6.1.82-1
CVE-2024-26697
	[bookworm] - linux 6.1.82-1
CVE-2024-26698
	[bookworm] - linux 6.1.82-1
CVE-2024-26700
	[bookworm] - linux 6.1.82-1
CVE-2024-26702
	[bookworm] - linux 6.1.82-1
CVE-2024-26704
	[bookworm] - linux 6.1.82-1
CVE-2024-26706
	[bookworm] - linux 6.1.82-1
CVE-2024-26707
	[bookworm] - linux 6.1.82-1
CVE-2024-26710
	[bookworm] - linux 6.1.82-1
CVE-2024-26712
	[bookworm] - linux 6.1.82-1
CVE-2024-26714
	[bookworm] - linux 6.1.82-1
CVE-2024-26715
	[bookworm] - linux 6.1.82-1
CVE-2024-26717
	[bookworm] - linux 6.1.82-1
CVE-2024-26718
	[bookworm] - linux 6.1.82-1
CVE-2024-26720
	[bookworm] - linux 6.1.82-1
CVE-2024-26722
	[bookworm] - linux 6.1.82-1
CVE-2024-26723
	[bookworm] - linux 6.1.82-1
CVE-2024-26726
	[bookworm] - linux 6.1.82-1
CVE-2024-26727
	[bookworm] - linux 6.1.82-1
CVE-2024-26731
	[bookworm] - linux 6.1.82-1
CVE-2024-26733
	[bookworm] - linux 6.1.82-1
CVE-2024-26735
	[bookworm] - linux 6.1.82-1
CVE-2024-26736
	[bookworm] - linux 6.1.82-1
CVE-2024-26737
	[bookworm] - linux 6.1.82-1
CVE-2024-26741
	[bookworm] - linux 6.1.82-1
CVE-2024-26742
	[bookworm] - linux 6.1.82-1
CVE-2024-26743
	[bookworm] - linux 6.1.82-1
CVE-2024-26744
	[bookworm] - linux 6.1.82-1
CVE-2024-26745
	[bookworm] - linux 6.1.82-1
CVE-2024-26747
	[bookworm] - linux 6.1.82-1
CVE-2024-26748
	[bookworm] - linux 6.1.82-1
CVE-2024-26749
	[bookworm] - linux 6.1.82-1
CVE-2024-26750
	[bookworm] - linux 6.1.82-1
CVE-2024-26751
	[bookworm] - linux 6.1.82-1
CVE-2024-26752
	[bookworm] - linux 6.1.82-1
CVE-2024-26753
	[bookworm] - linux 6.1.82-1
CVE-2024-26754
	[bookworm] - linux 6.1.82-1
CVE-2024-26759
	[bookworm] - linux 6.1.82-1
CVE-2024-26760
	[bookworm] - linux 6.1.82-1
CVE-2024-26761
	[bookworm] - linux 6.1.82-1
CVE-2024-26763
	[bookworm] - linux 6.1.82-1
CVE-2024-26764
	[bookworm] - linux 6.1.82-1
CVE-2024-26765
	[bookworm] - linux 6.1.82-1
CVE-2024-26766
	[bookworm] - linux 6.1.82-1
CVE-2024-26769
	[bookworm] - linux 6.1.82-1
CVE-2024-26771
	[bookworm] - linux 6.1.82-1
CVE-2024-26772
	[bookworm] - linux 6.1.82-1
CVE-2024-26773
	[bookworm] - linux 6.1.82-1
CVE-2024-26774
	[bookworm] - linux 6.1.82-1
CVE-2024-26775
	[bookworm] - linux 6.1.82-1
CVE-2024-26776
	[bookworm] - linux 6.1.82-1
CVE-2024-26777
	[bookworm] - linux 6.1.82-1
CVE-2024-26778
	[bookworm] - linux 6.1.82-1
CVE-2024-26779
	[bookworm] - linux 6.1.82-1
CVE-2024-26780
	[bookworm] - linux 6.1.82-1
CVE-2024-26781
	[bookworm] - linux 6.1.82-1
CVE-2024-26782
	[bookworm] - linux 6.1.82-1
CVE-2024-26787
	[bookworm] - linux 6.1.82-1
CVE-2024-26788
	[bookworm] - linux 6.1.82-1
CVE-2024-26789
	[bookworm] - linux 6.1.82-1
CVE-2024-26790
	[bookworm] - linux 6.1.82-1
CVE-2024-26791
	[bookworm] - linux 6.1.82-1
CVE-2024-26792
	[bookworm] - linux 6.1.82-1
CVE-2024-26793
	[bookworm] - linux 6.1.82-1
CVE-2024-26795
	[bookworm] - linux 6.1.82-1
CVE-2024-26798
	[bookworm] - linux 6.1.82-1
CVE-2024-26801
	[bookworm] - linux 6.1.82-1
CVE-2024-26802
	[bookworm] - linux 6.1.82-1
CVE-2024-26803
	[bookworm] - linux 6.1.82-1
CVE-2024-26804
	[bookworm] - linux 6.1.82-1
CVE-2024-26805
	[bookworm] - linux 6.1.82-1
CVE-2024-2004
	[bookworm] - curl 7.88.1-10+deb12u6
CVE-2024-2398
	[bookworm] - curl 7.88.1-10+deb12u6
CVE-2023-36328
	[bookworm] - libtommath 1.2.0-6+deb12u1
CVE-2023-50472
	[bookworm] - cjson 1.7.15-1+deb12u1
CVE-2023-50471
	[bookworm] - cjson 1.7.15-1+deb12u1
